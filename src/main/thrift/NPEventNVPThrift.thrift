namespace java com.sony.rnd.serializationtest

struct NPEventNVPThrift {
1: i64 eventId,
2: i32 eventCategoryID,
4: i32 eventType,
5: i64 eventDate,
6: string accountId,
7: string deviceId,
8: string ipAddress,
9: string categoryId,
10: string contentId,
11: string titleId,
12: string productId,
13: string entitlementId,
14: string sku,
15: string serviceId,
16: i64 orderId,
17: i64 transactionId,
18: map<string,string> attributes
}
