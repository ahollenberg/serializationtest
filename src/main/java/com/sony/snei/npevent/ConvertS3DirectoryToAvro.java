package com.sony.snei.npevent;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.zip.GZIPInputStream;



import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.internal.Constants;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ProgressEvent;
import com.amazonaws.services.s3.model.ProgressListener;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;

import java.util.regex.Pattern;
import java.util.regex.Matcher;





public class ConvertS3DirectoryToAvro {

	/**
	 * @param args
	 */
	
	
	static final int PROGRESSBAR_LENGTH = 20;
	
	private static String getHostIP(){
		Enumeration<NetworkInterface> nets;
		String ipAddress = null;
		try {
			nets = NetworkInterface.getNetworkInterfaces();
			for (NetworkInterface netint : Collections.list(nets)){
				
				Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
				for (InetAddress inetAddress : Collections.list(inetAddresses)) {
					if(netint.isLoopback() == Boolean.FALSE && inetAddress.isLinkLocalAddress() == Boolean.FALSE){
						ipAddress = inetAddress.toString().substring(1);
					}
		            
		        }
				
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//need exception handling in case no ip address is returned
		return ipAddress;
		
		
		
	}
	
	
	public static void drawProgressBar(int numerator, int denominator) {
	    int percent = (int) (((double) numerator / (double) denominator) * 100);

	    String bar = "[";
	    int lines = round((PROGRESSBAR_LENGTH * numerator) / denominator);
	    int blanks = PROGRESSBAR_LENGTH - lines;

	    for (int i = 0; i < lines; i++)
	        bar += "|";

	    for (int i = 0; i < blanks; i++)
	        bar += " ";

	    bar += "] " + percent + "%";

	    System.out.print(bar + "\r");
	}

	private static int round(double dbl) {
	    int noDecimal = (int) dbl;
	    double decimal = dbl - noDecimal;

	    if (decimal >= 0.5)
	        return noDecimal + 1;
	    else
	        return noDecimal;
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
	    System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " - START " );
	
		BufferedReader input = null; 
		String sInputLine = null;
	    int x = 0;
		ConvertEventTextStreamToAvro converter = new ConvertEventTextStreamToAvro();
		Pattern regexDatePattern = Pattern.compile("[0-9]{8}");
		Matcher regexMatcher = null;
		
		
		//Get AWS login info
		Properties props = new Properties();
        try {
        	props.load(ConvertS3DirectoryToAvro.class.getClassLoader().getResourceAsStream("AwsCredentials.properties"));
        }
        catch (IOException e) {
        	e.printStackTrace();
        }
        String myAccessKeyID = props.getProperty("accessKey");
        String mySecretKey = props.getProperty("secretKey");
		String sourceBucketName = props.getProperty("sourceBucketName");
		String targetBucketName = props.getProperty("targetBucketName");
		String sourcePrefix = props.getProperty("sourcePrefix");
		String targetPrefix = props.getProperty("targetPrefix");
		String tempSpacePrefix = props.getProperty("tempSpacePrefix");
        
		String outputFile = null;
		String dateOfInputFile = null;
		String s3Key = null;
        
        AWSCredentials myCredentials = new BasicAWSCredentials(myAccessKeyID, mySecretKey);
        AmazonS3 s3Client = new AmazonS3Client(myCredentials);
        TransferManager tx = new TransferManager(myCredentials);
        Upload targetUpload = null;
        
        
        
        
        try {
        	
        	
        	ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(sourceBucketName).withPrefix(sourcePrefix);
			ObjectListing objectListing;
			File output;
			
			
			
			do {
				objectListing = s3Client.listObjects(listObjectsRequest);
				
				for (S3ObjectSummary objectSummary : 
					objectListing.getObjectSummaries()) {
					System.out.println( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " - PROCESSING: " + objectSummary.getKey() + "  " + "(size = " + objectSummary.getSize() + ")");
					
		            S3Object s3object = s3Client.getObject(new GetObjectRequest(objectSummary.getBucketName(), objectSummary.getKey()));
		            
		            //create input stream
		            input = new BufferedReader(new InputStreamReader(new GZIPInputStream(s3object.getObjectContent())));
		            
		            //create output file
		            
		            regexMatcher = regexDatePattern.matcher(objectSummary.getKey());
		            if (regexMatcher.find()){
		            	dateOfInputFile = new String(regexMatcher.group());
		            }
		            else {
		            	System.out.println("No Match For Date in File Name");
		            	//raise exception here
		            }
		            
		            outputFile = tempSpacePrefix + "/das.np.evt.." + getHostIP() + "." + dateOfInputFile + "T" + new SimpleDateFormat("HHmm").format(new Date()) + ".avr.snappy";
		            //outputFile = "/media/data01/" + new File(objectSummary.getKey()).getName().substring(0, new File(objectSummary.getKey()).getName().indexOf(".")) + ".avro.snappy";
		            output = new File(outputFile);
		            
		            System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " - AVROCREATE: " + outputFile);
		            
		            //convert s3 object to avro file
		            ConvertEventTextStreamToAvro.convertStream(input, output);
		            System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " - AVROCONVERTDONE: " + outputFile);
		            
		            //upload file back to s3
		            s3Key = new String(targetPrefix + "/" + dateOfInputFile.substring(0, 4) + "/" + dateOfInputFile.substring(4, 6) + "/" + dateOfInputFile.substring(6, 8) + "/" + output.getName());

		            targetUpload = tx.upload(targetBucketName, s3Key, output);
		            
		            /*
		            targetUpload.addProgressListener(new ProgressListener() {
		                // This method is called periodically as your transfer progresses
		                public void progressChanged(ProgressEvent progressEvent) {
		                	drawProgressBar((int) targetUpload.getProgress().getPercentTransfered(),100);
		                }
		              }
		            
		            );
		            */
		            
		            while (targetUpload.isDone() == false) {
		            	drawProgressBar((int) targetUpload.getProgress().getPercentTransfered(),100);
		            	Thread.sleep(1000);
		            }
		            
		            //System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " - TSFRMGRPROP: " + tx.getConfiguration().getMinimumUploadPartSize() + " " + tx.getConfiguration().getMultipartUploadThreshold());
		            //s3Client.putObject(new PutObjectRequest(targetBucketName,s3Key,output));
		            System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " - UPLOADED: " + s3Key);
				}
				listObjectsRequest.setMarker(objectListing.getNextMarker());
			} while (objectListing.isTruncated());
        	
            
            
            
            
           
            
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which" +
            		" means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means"+
            		" the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
        finally{
			if (input != null){
				input.close();
			}
        }
	    System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " - END " );
    }

}
