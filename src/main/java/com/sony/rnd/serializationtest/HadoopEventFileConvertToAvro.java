package com.sony.rnd.serializationtest;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class HadoopEventFileConvertToAvro {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
		// TODO Auto-generated method stub
		String sourceFile = new String("hdfs://localhost.localdomain:8020/npevent/input/gzip/evt_event_20120528_1.part.dat.gz");
		String targetFile = new String("hdfs://localhost.localdomain:8020/npevent/output");
		
		Configuration conf = new Configuration();

		Path sourcePath = new Path(sourceFile);
		CompressionCodecFactory factory = new CompressionCodecFactory( conf); 
		CompressionCodec codec = factory.getCodec(sourcePath);

		if (codec == null) { 
			System.err.println(" No codec found for " + sourceFile); 
			System.exit( 1); 
	    }
		
		Path targetPath = new Path(targetFile);
		conf.setBoolean(" mapred.compress.map.output", true);
		conf.setClass(" mapred.map.output.compression.codec", SnappyCodec.class, CompressionCodec.class);
		//conf.setMapOutputCompressorClass(SnappyCodec.class);


		Job job = new Job(conf);
		
		
		
		FileInputFormat.addInputPath( job, sourcePath);
		FileOutputFormat.setOutputPath( job, targetPath);
		
		
		job.setJarByClass(HadoopEventFileConvertToAvro.class);
		job.setJobName("Test File Convert");
		
		job.setMapperClass(HadoopEventFileConvertToAvroMapper.class);
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(Text.class);
		
		job.setNumReduceTasks(0);
		
		job.setOutputKeyClass(LongWritable.class); 
		job.setOutputValueClass(Text.class);

		
		
		
		System.exit(job.waitForCompletion(true) ? 0 : 1);

		
	
	}

}
