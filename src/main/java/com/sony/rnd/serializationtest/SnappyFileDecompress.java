package com.sony.rnd.serializationtest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;



//for snappy compression
import org.xerial.snappy.Snappy;
import org.xerial.snappy.SnappyOutputStream;
import org.xerial.snappy.SnappyInputStream;



public class SnappyFileDecompress {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {
		String sInputLine = null;
		String input_file = new String("/media/data01/JSONEventNVP.json.snappy");
		String output_file = new String("/media/data01/JSONEventNVP.json");

		BufferedReader input = null;
		BufferedWriter output = null;
		
		input = new BufferedReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(input_file))));
		output = new BufferedWriter(new FileWriter(output_file));
		
		
		try{
			
		
			//loop through input file
			int x = 0;
			while (((sInputLine = input.readLine()) != null)){
				x++;
				//System.out.println(sInputLine);
			
				output.write(sInputLine);
				if ((x % 250000) == 0){
					output.flush();
				}
				
			}
		}
		finally{
			if (input != null){
				input.close();
			}
			if (output != null){
				output.close();
			}
			
		}
		
	}

}
