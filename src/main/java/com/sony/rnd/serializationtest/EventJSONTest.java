package com.sony.rnd.serializationtest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.GZIPInputStream;


import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

//for snappy compression
import org.xerial.snappy.Snappy;
import org.xerial.snappy.SnappyOutputStream;

public class EventJSONTest { 
	
	private static final Map<Integer, List<String>> EVENT_TYPE_TO_FIELDS = ImmutableMap.<Integer, List<String>>builder()
		      .put(100, ImmutableList.of("signin_id","ip_address","result","console_id","account_id","platform","additional_count","interval_seconds","title_id","referring_url","user_agent","web_entry_point"))
		      .put(101, ImmutableList.of("signin_id","ip_address","service_id","result","console_id","account_id","platform","additional_count","interval_seconds"))
		      .put(102, ImmutableList.of("signin_id","ip_address","console_id","entitlement_id","result","account_id","platform","additional_count","interval_seconds"))
		      .put(103, ImmutableList.of("signin_id","ip_address","session_id","result","platform","console_id","","additional_count","interval_seconds"))
		      .put(104, ImmutableList.of("session_id","signin_id","ip_address","service_id","result","platform","console_id","additional_count","interval_seconds"))
		      .put(423, ImmutableList.of("account_id","content_id","license_type","result","ip_address"))
		      .put(424, ImmutableList.of("account_id","console_id","content_id","result"))
		      .put(425, ImmutableList.of("account_id","content_id","license_type","result","ip_address","","","","console_id"))
		      .put(426, ImmutableList.of("account_id","content_id","reason","ip_address","timestamp","session_id","platform","","console_id","linkspeed"))
		      .put(427, ImmutableList.of("account_id","content_id","reason","timestamp","ip_address","session_id","platform","attempts","console_id"))
		      .put(428, ImmutableList.of("account_id","content_id","linkspeed","","ip_address","session_id","platform","","console_id","linkspeed_multiplier","asset_multiplier"))
		      .put(700, ImmutableList.of("account_id","session_id","product_id","platform","ip_address","console_id","base_category_id","title_id","referring_url"))
		      .put(701, ImmutableList.of("account_id","session_id","sku_id","platform","ip_address","console_id","recommended_seed_type","recommended_seed_id","experience_id","title_id","referring_url"))
		      .put(702, ImmutableList.of("account_id","session_id","category_id","","platform","console_id","ip_address","base_category_id"))
		      .put(703, ImmutableList.of("account_id","session_id","category_id","","language_code","image_url","index","location_id","from_category_id","platform","ip_address","console_id"))
		      .put(704, ImmutableList.of("account_id","session_id","sku_id","order_id","platform","storefront","buy_now","ip_address","console_id","recommended_seed_type","recommended_seed_id","experience_id","title_id","referring_url"))
		      .put(705, ImmutableList.of("account_id","session_id","sku_id","content_url","platform","ip_address","console_id"))
		      .build();
	
	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	
	
	public static void main(String[] args ) throws IOException,ParseException{
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		
		//input object pointers
		BufferedReader input = null; 
		
		//output object pointers
		JsonFactory f = new JsonFactory();
		JsonGenerator output = null;
		 
		List<String> fieldNames = null;
		
		//get properties for configuration
        Properties props = new Properties();
        try {
        	props.load(new FileInputStream("/home/sony/workspace/SerializationTest/src/main/properties/EventJSONTest.properties"));
        }
        catch (IOException e) {
        	e.printStackTrace();
        }
        String sourceFile = props.getProperty("input_file");
        String targetFile = props.getProperty("output_file");
        
        //determine need to compress output file to snappy
		String compress_prop = new String(props.getProperty("compress")).toLowerCase();
		boolean compress_flag;
		if (compress_prop.equals("true")){
			compress_flag = Boolean.TRUE;
		}
		else {
			compress_flag = Boolean.FALSE;
		}
		
		try{
			String sInputLine = null;
			String[] tokens;
			
			//open input file
			input = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(sourceFile))));
			
			
        	//compress output file to snappy if param is set in properties file
        	if (compress_flag == Boolean.TRUE){
        		output = f.createJsonGenerator(new SnappyOutputStream(new FileOutputStream(new File(targetFile + ".snappy"))));
        	}
        	else{
        		output = f.createJsonGenerator(new FileWriter(new File(targetFile)));
        	}
        	
        	
        	
			//loop through input file
			int x = 0;
			while (((sInputLine = input.readLine()) != null) && x <= 5000000){
			  x++;
			  
			  tokens = sInputLine.split(";");
			  
			  output.writeStartObject();
			  // handle event_id
			  output.writeNumberField("event_id", Long.valueOf(tokens[0]));
			  
			  // handle event_type
			  output.writeNumberField("event_type", Long.valueOf(tokens[2]));
			  
			  // handle event_date 
			  output.writeStringField("event_date", tokens[3]);
			  
			  fieldNames  = EVENT_TYPE_TO_FIELDS.get(Integer.valueOf(tokens[2]));
			  
			  
			  for (int y = 0; y < fieldNames.size();  y++){
				  output.writeStringField(fieldNames.get(y), tokens[y+4]);
			  }
			  
			  output.writeEndObject();
			  output.writeRaw("\n");
			  
			}
			
			
			if ((x % 250000) == 0){
				output.flush();
			}
			
			
			
			if (sInputLine == null){
				System.out.println("Finished Processing File");
			}
			else{
				System.out.println("Processed to line: " + x);
			}
			
			
			
		}
		finally{
			if (input != null){
				input.close();
			}
			if (output != null){
				output.close();
			}
			
		}
		
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	}
    /*
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		
		BufferedReader input = null;
		
		
		
		ObjectMapper mapper = new ObjectMapper();  
		User user = mapper.readValue(new File("/home/sony/workspace/SerializationTest/src/test/data/input/user.json"), User.class);
	    System.out.println(mapper.writeValueAsString(user));
	}
    */
}
