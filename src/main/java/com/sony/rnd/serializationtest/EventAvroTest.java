package com.sony.rnd.serializationtest;

import java.io.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;


//avro import
import org.apache.avro.Schema;
import org.apache.avro.Schema.Parser;
import org.apache.avro.file.CodecFactory;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;


//for java properties files
import java.util.Properties;


//for snappy compression
import org.xerial.snappy.Snappy;


//for logging
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EventAvroTest {
	
	private static Logger logger = LogManager.getLogger(EventAvroTest.class.getName());
	
	private static final Map<Integer, List<String>> EVENT_TYPE_TO_FIELDS = ImmutableMap.<Integer, List<String>>builder()
		      .put(100, ImmutableList.of("signin_id","ip_address","result","console_id","account_id","platform","additional_count","interval_seconds","title_id","referring_url","user_agent","web_entry_point"))
		      .put(101, ImmutableList.of("signin_id","ip_address","service_id","result","console_id","account_id","platform","additional_count","interval_seconds"))
		      .put(102, ImmutableList.of("signin_id","ip_address","console_id","entitlement_id","result","account_id","platform","additional_count","interval_seconds"))
		      .put(103, ImmutableList.of("signin_id","ip_address","session_id","result","platform","console_id","","additional_count","interval_seconds"))
		      .put(104, ImmutableList.of("session_id","signin_id","ip_address","service_id","result","platform","console_id","additional_count","interval_seconds"))
		      .put(423, ImmutableList.of("account_id","content_id","license_type","result","ip_address"))
		      .put(424, ImmutableList.of("account_id","console_id","content_id","result"))
		      .put(425, ImmutableList.of("account_id","content_id","license_type","result","ip_address","","","","console_id"))
		      .put(426, ImmutableList.of("account_id","content_id","reason","ip_address","timestamp","session_id","platform","","console_id","linkspeed"))
		      .put(427, ImmutableList.of("account_id","content_id","reason","timestamp","ip_address","session_id","platform","attempts","console_id"))
		      .put(428, ImmutableList.of("account_id","content_id","linkspeed","","ip_address","session_id","platform","","console_id","linkspeed_multiplier","asset_multiplier"))
		      .put(700, ImmutableList.of("account_id","session_id","product_id","platform","ip_address","console_id","base_category_id","title_id","referring_url"))
		      .put(701, ImmutableList.of("account_id","session_id","sku_id","platform","ip_address","console_id","recommended_seed_type","recommended_seed_id","experience_id","title_id","referring_url"))
		      .put(702, ImmutableList.of("account_id","session_id","category_id","","platform","console_id","ip_address","base_category_id"))
		      .put(703, ImmutableList.of("account_id","session_id","category_id","","language_code","image_url","index","location_id","from_category_id","platform","ip_address","console_id"))
		      .put(704, ImmutableList.of("account_id","session_id","sku_id","order_id","platform","storefront","buy_now","ip_address","console_id","recommended_seed_type","recommended_seed_id","experience_id","title_id","referring_url"))
		      .put(705, ImmutableList.of("account_id","session_id","sku_id","content_url","platform","ip_address","console_id"))
		      .build();
	
	private static void eventPColToAvro(NPEventNVP e, String colName, String pValue ) {
		

		
		//assign named columns in AVRO spec
		if (colName.equals("device_id") || colName.equals("console_id")) {
        	e.setDeviceId(pValue);
        }
        else if (colName.equals("account_id")) {
        	e.setAccountId(pValue);
        }
        else if (colName.equals("ip_address")) {
        	e.setIpAddress(pValue);
        }
        else if (colName.equals("category_id")) {
        	e.setCategoryId(pValue);
        }
        else if (colName.equals("content_id")) {
        	e.setContentId(pValue);
        }
        else if (colName.equals("entitlement_id")) {
        	e.setEntitlementId(pValue);
        }
        else if (colName.equals("product_id")) {
        	e.setProductId(pValue);
        }
        else if (colName.equals("sku") || colName.equals("sku_id")) {
        	e.setSku(pValue);
        }
        else if (colName.equals("title") || colName.equals("title_id")) {
        	e.setTitleId(pValue);
        }
        else if (colName.equals("service_id")) {
        	e.setServiceId(pValue);
        }
        else if (colName.equals("order_id")) {
        	e.setOrderId(Long.valueOf(pValue));
        }
        else if (colName.equals("transaction_id")) {
        	e.setTransactionId(Long.valueOf(pValue));
        }
		//assign un-named columns in AVRO spec to hashmap of name-value pairs
        else{
        	Map eventAttributes = e.getAttributes();
        	
        	if (eventAttributes == null){
        		eventAttributes = new HashMap<String, String>();
        		eventAttributes.put(colName, pValue);
        	}
        	else{
        		eventAttributes.put(colName, pValue);
        	}
        	e.setAttributes(eventAttributes);
        }
		
	}
	
	
	public static void main(String[] args ) throws IOException,ParseException{
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		
		logger.info("Info log");
		logger.entry("Entry log");
		logger.error("Error log");
		
		BufferedReader input = null;  
		NPEventNVP event = new NPEventNVP();  
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		List<String> fieldNames = null;
		DatumWriter<NPEventNVP> eventDatumWriter = null;
		DataFileWriter<NPEventNVP> dataFileWriter = null;
		
		//get properties for configuration
        Properties props = new Properties();
        try {
        	props.load(new FileInputStream("/home/sony/workspace/SerializationTest/src/main/properties/EventAvroTest.properties"));
        }
        catch (IOException e) {
        	e.printStackTrace();
        }
        String sourceFile = props.getProperty("input_file");
        String targetFile = props.getProperty("output_file");
        
        //determine need to compress output file to snappy
		String compress_prop = new String(props.getProperty("compress")).toLowerCase();
		boolean compress_flag;
		if (compress_prop.equals("true")){
			compress_flag = Boolean.TRUE;
		}
		else {
			compress_flag = Boolean.FALSE;
		}
		
		try{
			String sInputLine;
			String[] tokens;
			
			//open input file
			input = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(sourceFile))));
			
			//open output file
			eventDatumWriter = new SpecificDatumWriter<NPEventNVP>(NPEventNVP.class);
        	dataFileWriter = new DataFileWriter<NPEventNVP>(eventDatumWriter);
        	//compress output file to snappy if param is set in properties file
        	if (compress_flag == Boolean.TRUE){
        		dataFileWriter.setCodec(CodecFactory.snappyCodec());
        		dataFileWriter.create(event.getSchema(), new File(targetFile + ".snappy"));
        	}
        	else{
        		dataFileWriter.create(event.getSchema(), new File(targetFile));
        	}
        	
        	
        	
			//loop through input file
			int x = 0;
			while (((sInputLine = input.readLine()) != null)){
			  x++;
			  
			  tokens = sInputLine.split(";");
			  
			  event = new NPEventNVP();
			  event.setEventId(Long.valueOf(tokens[0]));
			  event.setEventType(Integer.valueOf(tokens[2]));
			  event.setEventDate(df.parse(tokens[3]).getTime());
			  
			  fieldNames  = EVENT_TYPE_TO_FIELDS.get(event.getEventType());
			  
			  
			  for (int y = 0; y < fieldNames.size();  y++){
				  eventPColToAvro(event,fieldNames.get(y),tokens[y+4]);
			  }
			  dataFileWriter.append(event);
			  if ((x % 250000) == 0){
				  dataFileWriter.flush();
			  }
			  
			}
			
			if (sInputLine == null){
				System.out.println("Finished Processing File");
			}
			else{
				System.out.println("Processed to line: " + x);
			}
			
			System.out.println("Processed to line: " + x);
			
		}
		
		finally{
			if (input != null){
				input.close();
			}
			if (dataFileWriter != null){
				dataFileWriter.close();
			}
			
		}
		
		
		
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	}

}
