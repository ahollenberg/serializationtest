package com.sony.rnd.serializationtest;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.GZIPInputStream;



import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;


//for thrift
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TIOStreamTransport;
import org.apache.thrift.TException;

//for snappy compression
import org.xerial.snappy.SnappyInputStream;
import org.xerial.snappy.SnappyOutputStream;


public class EventThriftReadTest {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws TException 
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException, TException {
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		
		BufferedInputStream input = null;  
		BufferedWriter output = null;
		NPEventNVPThrift event = new NPEventNVPThrift();  
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		TBinaryProtocol binaryIn = null;
		
		//get properties for configuration
        Properties props = new Properties();
        try {
        	props.load(new FileInputStream("/home/sony/workspace/SerializationTest/src/main/properties/EventThriftReadTest.properties"));
        }
        catch (IOException e) {
        	e.printStackTrace();
        }
        String sourceFile = props.getProperty("input_file");
        String targetFile = props.getProperty("output_file");
        
        //determine need to compress output file to snappy
		String compress_prop = new String(props.getProperty("compress")).toLowerCase();
		boolean compress_flag;
		if (compress_prop.equals("true")){
			compress_flag = Boolean.TRUE;
		}
		else {
			compress_flag = Boolean.FALSE;
		}
		
		try{
            //open input file
        	if (compress_flag == Boolean.TRUE){
        		input = new BufferedInputStream(new SnappyInputStream(new FileInputStream(sourceFile)));
        		binaryIn = new TBinaryProtocol(new TIOStreamTransport(input));
        	}
        	else{
        		input = new BufferedInputStream(new SnappyInputStream(new FileInputStream(sourceFile)));
        		binaryIn = new TBinaryProtocol(new TIOStreamTransport(input));
        	}
			
			
			//open output file
			output = new BufferedWriter(new FileWriter(targetFile));

			
			//loop through input file
			int x = 0;
			int bufferread = 0;
			System.out.println("starting loop");
			input.mark(1);
			while (input.read() != -1){
		      input.reset();
			  x++;
			  event.read(binaryIn);
			  System.out.println(event.toString());
			  
			  if ((x % 250000) == 0){
				  output.flush();
			  }
			  
			  input.mark(1);
			  
			}
			
			
			if (input.read() == -1){
				System.out.println("Finished Processing File");
			}
			else{
				System.out.println("Processed to line: " + x);
			}
			
			
		}
		finally{
			if (input != null){
				input.close();
			}
			if (output != null){
				output.close();
			}
			
		}
		
		
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	}

}
