package com.sony.rnd.serializationtest;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;

public class HadoopEventFileReadTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		String sourceDir = new String("hdfs://localhost.localdomain:8020/npevent/raw");
		
		Configuration conf = new Configuration();
		
		//get list of files in sourceDir
		
		FileSystem fs = FileSystem.get(URI.create(sourceDir), conf);
		Path sourcePath = new Path(sourceDir);
		FileStatus[] status = fs.listStatus(sourcePath);
		Path[] listedPaths = FileUtil.stat2Paths(status);
		
		for (Path p : listedPaths){
			System.out.println(p);
		}
	}

}
