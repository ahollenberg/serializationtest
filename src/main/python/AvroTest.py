import datetime
import avro.schema
from avro.datafile import DataFileReader, DataFileWriter
from avro.io import DatumReader, DatumWriter

print datetime.datetime.now()

schema = avro.schema.parse(open("/home/sony/workspace/SerializationTest/src/main/avro/NPEventNVP.avsc").read())

"""
writer = DataFileWriter(open("users.avro", "w"), DatumWriter(), schema)
writer.append({"name": "Alyssa", "favorite_number": 256})
writer.append({"name": "Ben", "favorite_number": 7, "favorite_color": "red"})
writer.close()
"""

reader = DataFileReader(open("/media/data01/AvroEventNVP.part.avro", "r"), DatumReader())
outputFile = open('/media/data01/AvroEventNVPPython.dat', 'w+')
x = 0
for event in reader:
    #print str(event);
    #outputFile.write(event['device_id'] + '\n');
    
    
    eventline = str(event);
    
    outputFile.write(eventline);
    x = x + 1
reader.close();
outputFile.close();

print x
print datetime.datetime.now()