import fastavro as avro
import datetime

print datetime.datetime.now()

outputFile = open('/media/data01/AvroEventNVPPython.dat', 'w+')
x = 0

with open('/media/data01/AvroEventNVP.part.avro', 'rb') as fo:
    reader = avro.reader(fo)
    schema = reader.schema

    for record in reader:
        #print str(record)
        #print record['device_id']
        outputFile.write(str(record))
        x = x + 1
    
outputFile.close();

print x
print datetime.datetime.now()